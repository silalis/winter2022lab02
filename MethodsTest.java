public class MethodsTest
{
	public static void main(String[] args)
	{
		int x = 10;
		/* System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x); 

		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50); 
		
		methodTwoInputNoReturn(2, 4.5); 
		
		int z = methodNoInputReturnInt();
		System.out.println(z); 
		
		double result = sumSquareRoot(6, 3);
		System.out.println(result); 
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length()); */
		
		int y = SecondClass.addOne(50);
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	} 
	
	public static void methodOneInputNoReturn(int cool)
	{
		System.out.println("Inside the method one input no return");
		System.out.println(cool);
	}
	
	public static void methodTwoInputNoReturn(int very, double nice)
	{
		System.out.println(very + " " + nice);
	}
	
	public static int methodNoInputReturnInt()
	{
		System.out.println("Inside the method no input return int");
		return 6;
	}
	
	public static double sumSquareRoot(int number1, int number2)
	{
		double temp = (double) number1 + (double) number2;
		double result = Math.sqrt(temp);
		
		return result;
	}
}