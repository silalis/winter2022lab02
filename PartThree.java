import java.util.Scanner;
public class PartThree
{
	public static void main (String[] args)
	{
		 Scanner reader = new Scanner(System.in); 
		 
		 System.out.println("Enter the length of the square:");
		 int length_square = reader.nextInt();
		 System.out.println("Enter the length of the rectangle:");
		 int length_rectangle = reader.nextInt();
		 System.out.println("Enter the width of the rectangle:");
		 int width_rectangle =  reader.nextInt();
		 
		 int area_square = AreaComputations.areaSquare(length_square);
		 System.out.println("The area of your square is: " + area_square);
		 AreaComputations ac = new AreaComputations();
		 System.out.println("The area of your rectangle is: " + ac.areaRectangle(length_rectangle, width_rectangle));	
	}	
}